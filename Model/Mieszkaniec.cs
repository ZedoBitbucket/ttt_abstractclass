﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public abstract class MieszkaniecAbstract
    {
        public abstract AdresAbstract Adres { get; set; }
        public abstract DaneOsoboweAbstract DaneOsobowe { get; set; }
    }

    public abstract class AdresAbstract
    {
        //  wersja 1
        public abstract int Wersja { get; }
        public abstract string Ulica { get; set; }
        public abstract string NrDomu { get; set; }
        //  wersja 2
        public abstract string Kod { get; set; }
        //  wersja 3
        public abstract string Dzielnica { get; set; }
    }

    public abstract class DaneOsoboweAbstract
    {
        public abstract int Wersja { get; protected set; }
        public abstract string Imię { get; set; }
        public abstract string Nazwisko { get; set; }
    }
}
