﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication
{
    class Program
    {
        static void Main(string[] args)
        {
            var mieszkaniec1 = new Mieszkaniec();
            mieszkaniec1.Adres = new AdresV1();

            var mieszkaniec2 = new Mieszkaniec();
            mieszkaniec2.Adres = new AdresV2();

            var mieszkaniec3 = new Mieszkaniec();
            mieszkaniec3.Adres = new AdresV3();
        }
    }

    public class Mieszkaniec : MieszkaniecAbstract
    {
        public override AdresAbstract Adres { get; set; }
        public override DaneOsoboweAbstract DaneOsobowe { get; set; }
    }

    public sealed class AdresV1 : AdresAbstract
    {
        public AdresV1()
        {
            this.NrDomu = "13";
            this.Ulica = "Dekabrystów";
        }

        public override string NrDomu { get; set; }
        public override string Ulica { get; set; }
        public override int Wersja { get { return 1; } }
        //  dodanie z drugiej wersji ale będzie null
        public override string Kod { get; set; }
        //  dodanie z trzeciej wersji ale będzie null
        public override string Dzielnica { get; set; }
    }

    public sealed class AdresV2 : AdresAbstract
    {
        public AdresV2()
        {
            this.NrDomu = "13";
            this.Ulica = "Dekabrystów";
            this.Kod = "45-061";
        }

        public override string NrDomu { get; set; }
        public override string Ulica { get; set; }
        public override int Wersja { get { return 2; } }
        //  wersja 2
        public override string Kod { get; set; }
        //  dodanie z trzeciej wersji ale będzie null
        public override string Dzielnica { get; set; }
    }

    public sealed class AdresV3 : AdresAbstract
    {
        public AdresV3()
        {
            this.NrDomu = "13";
            this.Ulica = "Dekabrystów";
            this.Kod = "45-061";
            this.Dzielnica = "Centrum";
        }

        public override string NrDomu { get; set; }
        public override string Ulica { get; set; }
        public override int Wersja { get { return 3; } }
        //  wersja 2
        public override string Kod { get; set; }
        //  wersja 3
        public override string Dzielnica { get; set; }
    }
}
